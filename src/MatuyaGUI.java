import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MatuyaGUI {
    private JPanel root;
    private JTabbedPane tabbedPane1;
    private JButton GyumesiButton;
    private JButton NegitamaGyumesiButton;
    private JButton CheeseGyumesiButton;
    private JTextPane orderedItemsList;
    private JTextPane textPane2;
    private JButton CheckoutButton;
    private JButton BibinButton;
    private JButton KimukaruButton;
    private JButton NegisioButton;
    private JButton ShougaButton;
    private JButton KarubiButton;
    private JButton HamburgeButton;
    private JButton Cancelbutton1;
    int i=0 ;
    int b,c;

    void order(String food){
        String currentText = orderedItemsList.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?",
                "Message",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0) {
            i += b;
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering "+food+". "+food+" costs "+b+"yen.",
                    "Message",
                    JOptionPane.PLAIN_MESSAGE);
            JOptionPane.showMessageDialog(null,
                    "Order for "+food+" received.",
                    "Message",
                    JOptionPane.PLAIN_MESSAGE);
            String NEW_TEXT = food+"  "+b+"yen";
            orderedItemsList.setText(currentText +food+"  "+b+"yen" + "\n");
            textPane2.setText(i+"yen");
        }
    }
    public MatuyaGUI() {
        GyumesiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)  {
                b=400;
                order("Gyumesi");


            }
        });
        NegitamaGyumesiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=580;
                order("Green onions and eggs Gyumesi");

            }
        });
        CheeseGyumesiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=590;
                order("Cheese Gyumesi");

            }
        });
        BibinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=600;
                order("Bibimbap Rice bowl");

            }
        });
        KimukaruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=610;
                order("Kimushi and Short ribs Rice bowl");


            }
        });
        NegisioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=620;
                order("Grilled beef with green onion salt  Rice bowl");


            }
        });
        ShougaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=750;
                order("Grilled ginger Set meal");


            }
        });
        KarubiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=830;
                order("Short ribs Set meal");


            }
        });
        HamburgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b=840;
                order("Hamburger steak Set meal");


            }
        });
        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Message",
                        JOptionPane.YES_NO_OPTION

                );
                if(confirmation==0){
                    int confirmation1 = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to take out?",
                            "Message",
                            JOptionPane.YES_NO_OPTION

                    );
                    if(confirmation1==1){
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is "+i+"yen.",
                                "Message",
                                JOptionPane.PLAIN_MESSAGE
                        );
                        i=0;

                    }
                    else if(confirmation1==0){
                        int c = i*54/55;
                        JOptionPane.showMessageDialog(null,
                                "Thank you. The total price is "+c+"yen.",
                                "Message",
                                JOptionPane.PLAIN_MESSAGE
                        );
                        orderedItemsList.setText("");
                        i=0;
                        textPane2.setText(i+"yen");
                    }
                }

            }
        });
        Cancelbutton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel?",
                        "Message",
                        JOptionPane.YES_NO_OPTION

                );
                if(confirmation==0) {
                    JOptionPane.showMessageDialog(null,
                            "I canceled them.",
                            "Message",
                            JOptionPane.PLAIN_MESSAGE
                    );
                    orderedItemsList.setText("");
                    i=0;
                    textPane2.setText(i+"yen");
                }

            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new MatuyaGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}






